package com.metweecs.testapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import java.util.HashMap;

public class DetailsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        Intent comingIntent = this.getIntent();
        HashMap<String,String> hmss = (HashMap<String, String>) comingIntent.getExtras().get("data");

        TextView tv1 = (TextView)findViewById(R.id.name);
        tv1.setText(hmss.get("name"));

        TextView tv2 = (TextView)findViewById(R.id.role);
        tv2.setText(hmss.get("role"));
    }
}

package com.metweecs.testapp;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    private ArrayList<JSONObject> jsonObjects;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        String json = loadPlayerData(this);
        try {
            JSONArray jsonArray = new JSONArray(json);
            jsonObjects = new ArrayList<JSONObject>();

            for(int i = 0; i < jsonArray.length(); i++){
                jsonObjects.add((JSONObject) jsonArray.get(i));
            }

            ImageButton capsBtn = findViewById(R.id.capsImage);

            capsBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        HashMap<String, String> capsData = getDataFromJSON("Caps");

                        Intent detailsActivity = new Intent(MainActivity.this, DetailsActivity.class);
                        detailsActivity.putExtra("data",capsData);
                        startActivity(detailsActivity);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });

            ImageButton soazBtn = findViewById(R.id.soazImage);

            soazBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        HashMap<String, String> soazData = getDataFromJSON("sOAz");


                        Intent detailsActivity = new Intent(MainActivity.this, DetailsActivity.class);
                        detailsActivity.putExtra("data",soazData);
                        startActivity(detailsActivity);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String loadPlayerData(Context context) {
        String json = null;
        try {
            InputStream is = context.getAssets().open("playerData.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }

        return json;
    }

    private HashMap<String, String> getDataFromJSON(String player) throws JSONException {

        HashMap<String, String> hmss = new HashMap<>();

        for (JSONObject jsentry:
                jsonObjects) {
            String name = jsentry.getString("name");

            if(name.equals("sOAz")){
                hmss.put("name", "sOAz");
                hmss.put("role", jsentry.getString("role"));
                return hmss;
            }
            else if(name.equals("Caps")){
                hmss.put("name", "Caps");
                hmss.put("role", jsentry.getString("role"));
                return hmss;
            }
        }


        throw new JSONException("");
    }
}
